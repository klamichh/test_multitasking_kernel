#include <avr/io.h>
#include <inttypes.h>

#include "avr_kernel.h"
#include "avr_kernel_defs.h"

#include "context.h"

volatile uint16_t stack[STACK_SIZE];
volatile uint16_t sp[4];
volatile int current_task = 0;

volatile uint16_t current_time = 0;

volatile int8_t task_ready[NUM_TASKS];
volatile uint16_t task_sleeping_until[NUM_TASKS] = {0};

void dispatcher();

void task_0();
void task_1();
void task_2();
void task_3();

uint16_t swap_endiannes (uint16_t x)
{
    return ((x & 0xFF) << 8) | ((x & 0xFF00) >> 8); 
}

void avr_kernel_init()
{
    int i;
    for (i = 0; i < NUM_TASKS; ++i)
    {
        task_ready[i] = 1;
    }

    stack[STACK_SIZE/4 - 2] = swap_endiannes(task_0);
    stack[STACK_SIZE/2 - 2] = swap_endiannes(task_1);
    stack[3*STACK_SIZE/4 - 2] = swap_endiannes(task_2);
    stack[STACK_SIZE - 2] = swap_endiannes(task_3);

    sp[0] = stack + STACK_SIZE/4 - 3;
    sp[1] = stack + STACK_SIZE/2 - 3;
    sp[2] = stack + 3*STACK_SIZE/4 - 3;
    sp[3] = stack + STACK_SIZE - 3;

    SP = sp[1];
    SAVE_CONTEXT;
    sp[1] = SP;

    SP = sp[2];
    SAVE_CONTEXT;
    sp[2] = SP;

    SP = sp[3];
    SAVE_CONTEXT;
    sp[3] = SP;

    current_task = 0;
    SP = sp[current_task];
}

void yield()
{
    dispatcher();
}

void sleep (uint16_t ticks)
{
	
    if (ticks != 0)
    {
        task_ready[current_task] = 0;
        task_sleeping_until[current_task] = current_time + ticks;
    }
    dispatcher();
}

void dispatcher()
{
    SAVE_CONTEXT;
    sp[current_task] = SP;

    do
    {
        current_task = ((current_task + 1) & (NUM_TASKS-1));
    }
    while (!task_ready[current_task]);

    SP = sp[current_task];
    RESTORE_CONTEXT;
}

void timer()
{
    int i;
    ++current_time;
    for (i = 0; i < NUM_TASKS; ++i)
    {
        if (task_sleeping_until[i] == current_time)
        {
            task_ready[i] = 1;
        }
    }
}
