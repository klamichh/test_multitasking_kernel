#ifndef AVR_KERNEL_H
#define AVR_KERNEL_H

#include <inttypes.h>

void avr_kernel_init();
void yield();
void dispatcher();

void sleep (uint16_t ticks);

#define IDLE_STACK_SIZE 64

#endif

